package it.simonecascino.pokemonviewer.data.repositories

import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.daos.StatDao
import it.simonecascino.pokemonviewer.data.models.Stat
import org.koin.core.KoinComponent
import org.koin.core.inject

interface PokemonStatsRepository {
    val stats: Observable<List<Stat>>
}

class PokemonStatsRepositoryImpl(id: Long): PokemonStatsRepository, KoinComponent{

    private val statDao: StatDao by inject()

    override val stats = statDao.getStatsById(id).map {
        it.map { it.toBaseModel() }
    }

}