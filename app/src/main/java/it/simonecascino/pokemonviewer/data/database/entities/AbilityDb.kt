package it.simonecascino.pokemonviewer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import it.simonecascino.pokemonviewer.data.models.Ability

@Entity(tableName = "ability")
data class AbilityDb (
    @PrimaryKey @ColumnInfo(name = "ability_id") val id: Long,
    val name: String,
    val slot: Int,
    @ColumnInfo(name = "is_hidden") val isHidden: Boolean
) {
    fun toBaseModel() = Ability(id, name, slot, isHidden)

    fun toPokemonAbilityModel(pokemonId: Long) = PokemonAbilityDb(id, pokemonId)
}

@Entity(tableName = "pokemon_ability",
    primaryKeys = ["ability_ref_id", "pokemon_ref_id"]
)
data class PokemonAbilityDb(
    @ColumnInfo(name = "ability_ref_id") val abilityId: Long,
    @ColumnInfo(name = "pokemon_ref_id") val pokemonId: Long
)