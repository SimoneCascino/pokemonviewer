package it.simonecascino.pokemonviewer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import it.simonecascino.pokemonviewer.data.models.Pokemon
import it.simonecascino.pokemonviewer.data.models.PokemonDetails

@Entity(tableName = "pokemon")
data class PokemonDb(
    @PrimaryKey @ColumnInfo(name = "pokemon_id") val id: Long,
    val name: String,
    @Embedded
    val details: PokemonDetailsDb?
){

    fun toBaseModel(): Pokemon {
        return Pokemon(id, name)
    }

    fun toDetailModel(): PokemonDetails{

        return PokemonDetails(
            toBaseModel(),
            details?.let {
                PokemonDetails.Details(
                    it.baseExperience,
                    it.height,
                    it.isDefault,
                    it.locationAreaEncounters,
                    it.order,
                    it.weight,
                    PokemonDetails.Details.Images(
                        it.images.backDefault,
                        it.images.backFemale,
                        it.images.backShiny,
                        it.images.backShinyFemale,
                        it.images.frontDefault,
                        it.images.frontFemale,
                        it.images.frontShiny,
                        it.images.frontShinyFemale,
                        it.images.officialArtwork
                    )
                )
            }
        )
    }

}

data class PokemonDetailsDb(
    @ColumnInfo(name = "base_experience") val baseExperience: Int,
    val height: Int,
    @ColumnInfo(name = "is_default") val isDefault: Boolean,
    @ColumnInfo(name = "location_area_encounters") val locationAreaEncounters: String,
    val order: Int,
    val weight: Int,

    @Embedded
    val images: ImagesDb
){
    data class ImagesDb(
        @ColumnInfo(name = "back_default") val backDefault: String?,
        @ColumnInfo(name = "back_female") val backFemale: String?,
        @ColumnInfo(name = "back_shiny") val backShiny: String?,
        @ColumnInfo(name = "back_shiny_female") val backShinyFemale: String?,
        @ColumnInfo(name = "front_default") val frontDefault: String?,
        @ColumnInfo(name = "front_female") val frontFemale: String?,
        @ColumnInfo(name = "front_shiny") val frontShiny: String?,
        @ColumnInfo(name = "front_shiny_female") val frontShinyFemale: String?,
        @ColumnInfo(name = "official_artwork") val officialArtwork: String?
    )
}

data class SimplePokemonDb(
    @ColumnInfo("pokemon_id")val id: Long,
    val name: String){

    fun toBaseModel(): Pokemon {
        return Pokemon(id, name)
    }

}

fun List<SimplePokemonDb>.toPageKeys(previousKey: Int?, nextKey: Int?): List<PageKeyDb>{
    return map {
        PageKeyDb(
            it.id,
            previousKey = previousKey,
            nextKey = nextKey,
        )
    }
}