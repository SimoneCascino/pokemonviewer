package it.simonecascino.pokemonviewer.data.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

fun provideLoggingInterceptor(): HttpLoggingInterceptor{
    return HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}

fun provideHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient{
    return OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()
}

fun provideConverterFactory(): Converter.Factory{
    return MoshiConverterFactory.create(
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build())
}

fun provideRetrofit(converterFactory: Converter.Factory, httpClient: OkHttpClient): Retrofit{
    return Retrofit.Builder()
        .baseUrl("https://pokeapi.co/api/v2/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(converterFactory)
        .client(httpClient)
        .build()
}

fun provideApi(retrofit: Retrofit): Api{
    return retrofit.create(Api::class.java)
}