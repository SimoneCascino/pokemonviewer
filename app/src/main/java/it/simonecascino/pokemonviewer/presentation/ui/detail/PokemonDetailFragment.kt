package it.simonecascino.pokemonviewer.presentation.ui.detail

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.MaterialContainerTransform
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentPokemonDetailBinding
import it.simonecascino.pokemonviewer.presentation.ui.detail.abilities.PokemonAbilitiesFragment
import it.simonecascino.pokemonviewer.presentation.ui.detail.moves.PokemonMovesFragment
import it.simonecascino.pokemonviewer.presentation.ui.detail.stats.PokemonStatsFragment
import it.simonecascino.pokemonviewer.presentation.utils.observe
import it.simonecascino.pokemonviewer.presentation.utils.setVisible
import it.simonecascino.pokemonviewer.presentation.utils.snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class PokemonDetailFragment : Fragment() {

    private lateinit var binding: FragmentPokemonDetailBinding

    private val args: PokemonDetailFragmentArgs by navArgs()

    private val pokemonDetailViewModel: PokemonDetailViewModel by viewModel{ parametersOf(args.pokemon.id) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()

        sharedElementEnterTransition = MaterialContainerTransform().apply {
            drawingViewId = R.id.navHost
            duration = 350L
            scrimColor = Color.TRANSPARENT
            setAllContainerColors(ContextCompat.getColor(requireActivity(), R.color.color_surface))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        (requireActivity() as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)

        NavigationUI.setupWithNavController(binding.collapsingToolbarLayout, binding.toolbar, navController, appBarConfiguration)

        binding.collapsingToolbarLayout.title = args.pokemon.displayName

        val pagerAdapter = DetailPagerAdapter(this, listOf(
            PokemonStatsFragment.createInstance(args.pokemon.id),
            PokemonAbilitiesFragment.createInstance(args.pokemon.id),
            PokemonMovesFragment.createInstance(args.pokemon.id)
        ))

        binding.viewPager.adapter = pagerAdapter

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = when(position){
                0 -> getString(R.string.pokemon_detail_tab_stats)
                1 -> getString(R.string.pokemon_detail_tab_abilities)
                else -> getString(R.string.pokemon_detail_tab_moves)
            }
        }.attach()

        observe(pokemonDetailViewModel.observableState){ state ->

            if(state.showGallery) {

                if(state.imagesAvailable())
                    findNavController().navigate(PokemonDetailFragmentDirections.actionPokemonDetailFragmentToGalleryFragment(
                        state.value!!.details!!.images,
                        state.value.pokemon.displayName
                    ))

                pokemonDetailViewModel.dispatch(Action.HandleShowGallery)
            }

            else renderView(state)
        }


    }

    private fun renderView(state: DetailState){

        binding.pokemon = state.value

        if(state.imagesAvailable()){

            state.value?.details?.images?.officialArtwork?.let { url ->

                Glide.with(this).load(url).addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        startPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        startPostponedEnterTransition()
                        return false
                    }

                }) .into(binding.imageView)

            }

        }

        if(binding.baseExperienceView.isInvisible && state.value?.details != null){

            view?.post {
                binding.baseExperienceView.setVisible()
                binding.orderView.setVisible()
                binding.heightView.setVisible()
                binding.weightView.setVisible()
            }

        }

        if(state.isError){

            startPostponedEnterTransition()

            view?.snackbar(getString(R.string.error_connection), getString(R.string.snackbar_retry)){
                pokemonDetailViewModel.dispatch(Action.GetDetail)
            }

        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_gallery, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        pokemonDetailViewModel.dispatch(Action.ShowGallery)
        return true
    }

}