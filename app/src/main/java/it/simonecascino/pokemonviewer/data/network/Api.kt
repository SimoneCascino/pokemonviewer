package it.simonecascino.pokemonviewer.data.network

import io.reactivex.Single
import it.simonecascino.pokemonviewer.data.PAGE_SIZE
import it.simonecascino.pokemonviewer.data.network.pojos.PokemonApiResponse
import it.simonecascino.pokemonviewer.data.network.pojos.PokemonDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("pokemon")
    fun getPokemon(@Query("offset") offset: Int = 0,
                   @Query("limit") limit: Int = PAGE_SIZE): Single<PokemonApiResponse>

    @GET("pokemon/{id}")
    fun getPokemonDetail(@Path("id") id: Long): Single<PokemonDetailResponse>

}