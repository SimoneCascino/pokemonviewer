package it.simonecascino.pokemonviewer.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import it.simonecascino.pokemonviewer.data.database.daos.*
import it.simonecascino.pokemonviewer.data.database.entities.*

@Database(entities = [
    PokemonDb::class,
    PageKeyDb::class,
    StatDb::class,
    AbilityDb::class,
    PokemonAbilityDb::class,
    MoveDb::class,
    PokemonMoveDb::class],
    version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase(){
    abstract fun pokemonDao(): PokemonDao
    abstract fun pageKeyDao(): PageKeyDao
    abstract fun statDao(): StatDao
    abstract fun abilityDao(): AbilityDao
    abstract fun pokemonAbilityDao(): PokemonAbilityDao
    abstract fun moveDao(): MoveDao
    abstract fun pokemonMoveDao(): PokemonMoveDao
}