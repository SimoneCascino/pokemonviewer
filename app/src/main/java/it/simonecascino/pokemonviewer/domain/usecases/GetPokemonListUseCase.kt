package it.simonecascino.pokemonviewer.domain.usecases

import androidx.paging.PagingData
import io.reactivex.Flowable
import it.simonecascino.pokemonviewer.data.repositories.PokemonRepository
import it.simonecascino.pokemonviewer.data.models.Pokemon

class GetPokemonListUseCase(private val repository: PokemonRepository) {
    fun getPokemon(): Flowable<PagingData<Pokemon>> = repository.getPokemon()
}