package it.simonecascino.pokemonviewer.data.repositories

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.database.AppDatabase
import it.simonecascino.pokemonviewer.data.database.entities.AbilityDb
import it.simonecascino.pokemonviewer.data.database.entities.MoveDb
import it.simonecascino.pokemonviewer.data.database.entities.PokemonDb
import it.simonecascino.pokemonviewer.data.database.entities.StatDb
import it.simonecascino.pokemonviewer.data.models.PokemonDetails
import it.simonecascino.pokemonviewer.data.network.Api
import it.simonecascino.pokemonviewer.data.network.pojos.PokemonDetailResponse
import org.koin.core.KoinComponent
import org.koin.core.inject

interface PokemonDetailRepository {
    val pokemon: Observable<PokemonDetails>
    val pokemonApi: Single<PokemonDetailResponse>
}

class PokemonDetailRepositoryImpl(id: Long): PokemonDetailRepository, KoinComponent{

    private val api: Api by inject()
    private val database: AppDatabase by inject()

    override val pokemon = database.pokemonDao().getPokemon(id).map {
        it.toDetailModel()
    }

    override val pokemonApi = api.getPokemonDetail(id).subscribeOn(Schedulers.io()).map { pokemon ->

        save(pokemon.toDbModel(),
            pokemon.stats.map { it.toDbModel(pokemon.id) },
            pokemon.abilities.map { it.toDbModel() },
            pokemon.moves.map { it.toDbModel() })
        pokemon
    }


    private fun save(pokemonDb: PokemonDb, stats: List<StatDb>, abilities: List<AbilityDb>, moves: List<MoveDb>){

        database.runInTransaction {
            database.pokemonDao().updatePokemon(pokemonDb)
            database.statDao().insertStats(stats)
            database.abilityDao().insertAbilities(abilities)
            database.pokemonAbilityDao().insertKeys(abilities.map {it.toPokemonAbilityModel(pokemonDb.id)})
            database.moveDao().insertMove(moves)
            database.pokemonMoveDao().insertPokemonMove(moves.map { it.toPokemonMoveModel(pokemonDb.id) })
        }
    }

}