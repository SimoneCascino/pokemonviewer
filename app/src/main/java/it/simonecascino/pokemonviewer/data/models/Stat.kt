package it.simonecascino.pokemonviewer.data.models

import java.util.*

data class Stat(
    val id: Long,
    val name: String,
    val baseStat: Int,
    val effort: Int
){

    val displayName = name.capitalize(Locale.getDefault())

}