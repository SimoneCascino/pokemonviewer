package it.simonecascino.pokemonviewer.presentation.ui.detail.moves

import com.ww.roxie.BaseAction
import com.ww.roxie.BaseState
import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.models.Move
import it.simonecascino.pokemonviewer.domain.usecases.GetPokemonMovesUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class PokemonMovesViewModel(private val id: Long): BaseViewModel<Action, MoveState>(), KoinComponent {

    override val initialState = MoveState()

    private val getPokemonMovesUseCase: GetPokemonMovesUseCase by inject{ parametersOf(id) }

    private val reducer: Reducer<MoveState, Change> = { state, change ->
        when (change) {
            is Change.Loading -> state.copy(
                isIdle = false,
                isLoading = true
            )
            is Change.Moves -> state.copy(
                isLoading = false,
                moves = change.moves
            )
            is Change.Error -> state.copy(
                isLoading = false
            )
        }
    }

    private fun bindActions(){

        val moveChange = actions.ofType<Action.GetMoves>()
            .switchMap {
                getPokemonMovesUseCase.moves
                    .subscribeOn(Schedulers.io())
                    .map<Change> { Change.Moves(it)}
                    .defaultIfEmpty(Change.Moves(emptyList()))
                    .startWith(Change.Loading)
            }

        disposables += moveChange
            .scan(initialState, reducer)
            .filter{ !it.isIdle }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

    }

    init {
        bindActions()
        dispatch(Action.GetMoves)
    }

}

sealed class Action: BaseAction {
    object GetMoves: Action()
}

sealed class Change {
    object Loading: Change()
    data class Moves(val moves: List<Move>): Change()
    data class Error(val throwable: Throwable?) : Change()
}

data class MoveState(
    val moves: List<Move> = listOf(),
    val isIdle: Boolean = true,
    val isLoading: Boolean = false
) : BaseState