package it.simonecascino.pokemonviewer.presentation.ui.detail.abilities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.data.models.Ability
import it.simonecascino.pokemonviewer.databinding.ItemAbilityBinding

class PokemonAbilitiesAdapter: ListAdapter<Ability, PokemonAbilitiesAdapter.ViewHolder>(AbilityDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(private val binding: ItemAbilityBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_ability, parent, false
                )
            )
        }

        fun bind(ability: Ability){
            binding.ability = ability
            binding.executePendingBindings()
        }

    }

    private class AbilityDiff: DiffUtil.ItemCallback<Ability>(){
        override fun areItemsTheSame(oldItem: Ability, newItem: Ability) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Ability, newItem: Ability) = oldItem == newItem
    }

}