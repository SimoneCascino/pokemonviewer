package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import it.simonecascino.pokemonviewer.data.database.entities.PokemonAbilityDb

@Dao
interface PokemonAbilityDao{

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertKeys(keys: List<PokemonAbilityDb>)

}