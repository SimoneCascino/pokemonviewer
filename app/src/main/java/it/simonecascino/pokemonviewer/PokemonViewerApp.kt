package it.simonecascino.pokemonviewer

import android.app.Application
import it.simonecascino.pokemonviewer.data.di.databaseModule
import it.simonecascino.pokemonviewer.data.di.networkModule
import it.simonecascino.pokemonviewer.data.di.remoteMediatorModule
import it.simonecascino.pokemonviewer.domain.di.domainModule
import it.simonecascino.pokemonviewer.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class PokemonViewerApp: Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@PokemonViewerApp)
            modules(presentationModule,
                domainModule,
                networkModule,
                databaseModule,
                remoteMediatorModule)
        }
    }

}