package it.simonecascino.pokemonviewer.presentation.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.transition.MaterialElevationScale
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentPokemonListBinding
import it.simonecascino.pokemonviewer.presentation.custom.SpaceItemDecoration
import it.simonecascino.pokemonviewer.presentation.utils.dpToPx
import it.simonecascino.pokemonviewer.presentation.utils.observe
import org.koin.android.viewmodel.ext.android.viewModel

class PokemonListFragment : Fragment() {

    private val pokemonListViewModel: PokemonListViewModel by viewModel()

    private lateinit var binding: FragmentPokemonListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        postponeEnterTransition()

        view.doOnPreDraw {
            startPostponedEnterTransition()
        }

        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        val adapter = PokemonListAdapter(PokemonListAdapter.ItemClickListener { pokemon, imageView ->

            exitTransition = MaterialElevationScale(false).apply {
                duration = 350L
            }
            reenterTransition = MaterialElevationScale(true).apply {
                duration = 350L
            }

            val extras = FragmentNavigatorExtras(
                imageView to imageView.transitionName
            )

            findNavController().navigate(
                PokemonListFragmentDirections.actionPokemonListFragmentToPokemonDetailFragment(pokemon),
                extras
            )
        })

        val loadStateAdapter = PokemonListLoadAdapter{
            adapter.retry()
        }

        (binding.pokemonList.layoutManager as? GridLayoutManager)?.let { layoutManager ->
            layoutManager.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup(){

                override fun getSpanSize(position: Int): Int {

                    val loadState = loadStateAdapter.loadState

                    if(position == layoutManager.itemCount -1 && loadState !is LoadState.NotLoading)
                        return layoutManager.spanCount

                    return 1
                }

            }
        }

        binding.pokemonList.addItemDecoration(SpaceItemDecoration(requireActivity().dpToPx(16)))
        binding.pokemonList.adapter = adapter.withLoadStateFooter(loadStateAdapter)

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        observe(pokemonListViewModel.observableState){state ->
            adapter.submitData(lifecycle, state.pagedPokemons)
        }

    }

    override fun onDetach() {
        super.onDetach()



    }


}