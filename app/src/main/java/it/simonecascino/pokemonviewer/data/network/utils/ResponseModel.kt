package it.simonecascino.pokemonviewer.data.network.utils

import android.net.Uri
import it.simonecascino.pokemonviewer.data.database.utils.DbModel

interface ResponseModel <out T: DbModel<*>> {
    fun toDbModel(): T
}

interface ResponseModelContainer <out T: List<DbModel<*>>>{
    fun toDbModel(): T
}

fun Uri.extractId(): Long{

    val extractedValue = runCatching { lastPathSegment?.toLong() }

    return if(extractedValue.isSuccess) extractedValue.getOrNull() ?: -1
    else -1

}