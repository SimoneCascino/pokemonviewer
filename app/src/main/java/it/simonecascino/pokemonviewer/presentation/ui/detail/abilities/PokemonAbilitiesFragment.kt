package it.simonecascino.pokemonviewer.presentation.ui.detail.abilities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentPokemonAbilitiesBinding
import it.simonecascino.pokemonviewer.presentation.custom.SpaceItemDecoration
import it.simonecascino.pokemonviewer.presentation.utils.observe
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

private const val KEY_ID = "id"

class PokemonAbilitiesFragment : Fragment() {

    private val pokemonAbilitiesViewModel: PokemonAbilitiesViewModel by viewModel{ parametersOf(arguments?.getLong(
        KEY_ID) ?: -1)}

    private lateinit var binding: FragmentPokemonAbilitiesBinding

    companion object{

        @JvmStatic
        fun createInstance(id: Long) = PokemonAbilitiesFragment().apply {
            arguments = Bundle(1).apply {
                putLong(KEY_ID, id)
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_abilities, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PokemonAbilitiesAdapter()

        binding.abilitiesList.addItemDecoration(SpaceItemDecoration(16))
        binding.abilitiesList.adapter = adapter

        observe(pokemonAbilitiesViewModel.observableState){
            adapter.submitList(it.abilities)
        }

    }
}