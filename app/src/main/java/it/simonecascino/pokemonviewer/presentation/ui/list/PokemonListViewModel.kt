package it.simonecascino.pokemonviewer.presentation.ui.list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.ww.roxie.BaseAction
import com.ww.roxie.BaseState
import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.models.Pokemon
import it.simonecascino.pokemonviewer.domain.usecases.GetPokemonListUseCase
import timber.log.Timber

class PokemonListViewModel(
    initialState: ListViewState?,
    private val loadPokemonListUseCase: GetPokemonListUseCase
): BaseViewModel<Action, ListViewState>() {

    override val initialState: ListViewState = initialState ?: ListViewState(isIdle = true)

    private val reducer: Reducer<ListViewState, Change> = { state, change ->
        when (change) {
            is Change.GetPagingData -> state.copy(
                isIdle = false,
                pagedPokemons = change.values
            )
        }
    }

    private fun bindActions() {

        val loadPokemons = actions.ofType<Action.LoadPokemon>().switchMap {
            loadPokemonListUseCase.getPokemon()
                .cachedIn(viewModelScope)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .map<Change> {
                    Change.GetPagingData(it)
                }
        }

        disposables += loadPokemons
            .scan(initialState, reducer)
            .filter{
                !it.isIdle
            }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

        /*

        val loadNotesChange = actions.ofType<Action.LoadPokemon>()
            .switchMap {
                loadPokemonListUseCase.getPokemon()
                    .subscribeOn(Schedulers.io())
                    .map<Change>{ pokemonApiResponse ->
                        Change.Pokemons(pokemonApiResponse.results.map {
                            Pokemon(it.extractId(), it.name)
                        })
                    }
                    .defaultIfEmpty(
                        Change.Pokemons(emptyList())
                    )
                    .onErrorReturn {
                        Change.Error(it)
                    }
                    .startWith(Change.Loading)
            }

        disposables += loadNotesChange
            .scan(initialState, reducer)
            .filter { !it.isIdle }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

         */

    }

    init{
        bindActions()
        dispatch(Action.LoadPokemon)
    }

}

sealed class Action: BaseAction{
    object LoadPokemon: Action()
}

sealed class Change {
    data class GetPagingData(val values: PagingData<Pokemon>): Change()
}

data class ListViewState(
    val pagedPokemons: PagingData<Pokemon> = PagingData.empty(),
    val isIdle: Boolean = true
) : BaseState