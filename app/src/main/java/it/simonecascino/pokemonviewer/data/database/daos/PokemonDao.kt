package it.simonecascino.pokemonviewer.data.database.daos

import androidx.paging.PagingSource
import androidx.room.*
import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.entities.PokemonDb
import it.simonecascino.pokemonviewer.data.database.entities.SimplePokemonDb

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE, entity = PokemonDb::class)
    fun insertPokemons(pokemons: List<SimplePokemonDb>)

    @Query("select pokemon_id, name from pokemon")
    fun getPokemons(): PagingSource<Int, SimplePokemonDb>

    @Query("delete from pokemon")
    fun deletePokemons()

    @Transaction
    @Query("select * from pokemon where pokemon_id = :id")
    fun getPokemon(id: Long): Observable<PokemonDb>

    @Update(entity = PokemonDb::class)
    fun updatePokemon(pokemon: PokemonDb)

}