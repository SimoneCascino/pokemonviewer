package it.simonecascino.pokemonviewer.data.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import androidx.paging.rxjava2.flowable
import io.reactivex.Flowable
import it.simonecascino.pokemonviewer.data.PAGE_SIZE
import it.simonecascino.pokemonviewer.data.PokemonListRemoteMediator
import it.simonecascino.pokemonviewer.data.models.Pokemon

interface PokemonRepository  {
    fun getPokemon(): Flowable<PagingData<Pokemon>>
}

class PokemonRepositoryImpl(private val remoteMediator: PokemonListRemoteMediator):
    PokemonRepository {

    override fun getPokemon(): Flowable<PagingData<Pokemon>> {
        return Pager(

            config = PagingConfig(
                pageSize = PAGE_SIZE,
                initialLoadSize = 120
            ),



            remoteMediator = remoteMediator,

            pagingSourceFactory = { remoteMediator.getPagingSourceFactory() }

        ).flowable.map {
            it.map {
                it.toBaseModel()
            }
        }
    }

}