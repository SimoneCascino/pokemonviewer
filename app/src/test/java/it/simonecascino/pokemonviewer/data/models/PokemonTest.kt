package it.simonecascino.pokemonviewer.data.models

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.core.Is.`is`
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class PokemonTest{

    private val pokemon = Pokemon(10, "Pokemon")

    @Test
    fun `check if transition name coontains the id`(){
        assertThat(pokemon.transitionName.contains(pokemon.id.toString()), `is`(true))
    }

    @Test
    fun `check if artwork contains the id`(){
        assertThat(pokemon.artworkUrl.contains(pokemon.id.toString()), `is`(true))
    }

}