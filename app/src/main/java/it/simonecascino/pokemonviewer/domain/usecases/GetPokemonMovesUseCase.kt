package it.simonecascino.pokemonviewer.domain.usecases

import it.simonecascino.pokemonviewer.data.repositories.PokemonMovesRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class GetPokemonMovesUseCase(id: Long): KoinComponent{

    private val repository: PokemonMovesRepository by inject { parametersOf(id) }

    val moves = repository.moves

}