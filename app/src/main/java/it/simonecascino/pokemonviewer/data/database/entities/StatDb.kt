package it.simonecascino.pokemonviewer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import it.simonecascino.pokemonviewer.data.models.Stat

@Entity(tableName = "stat",
    foreignKeys = [
        ForeignKey(
            entity = PokemonDb::class,
            parentColumns = arrayOf("pokemon_id"),
            childColumns = arrayOf("pokemon_stat_id"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class StatDb(
    @ColumnInfo(name = "pokemon_stat_id", index = true) val pokemonId: Long,
    val name: String,
    @ColumnInfo(name = "base_stat") val baseStat: Int,
    val effort: Int,
    @PrimaryKey(autoGenerate = true) val id: Long = 0
){

    fun toBaseModel() = Stat(
        id, name, baseStat, effort
    )

}