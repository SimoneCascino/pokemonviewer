package it.simonecascino.pokemonviewer.presentation.ui.detail.moves

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.data.models.Move
import it.simonecascino.pokemonviewer.databinding.ItemMoveBinding

class PokemonMovesListAdapter: ListAdapter<Move, PokemonMovesListAdapter.ViewHolder>(MoveDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(private val binding: ItemMoveBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_move, parent, false
                )
            )
        }

        fun bind(move: Move){
            binding.move = move
            binding.executePendingBindings()
        }
    }

    private class MoveDiff: DiffUtil.ItemCallback<Move>(){
        override fun areItemsTheSame(oldItem: Move, newItem: Move) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Move, newItem: Move) = oldItem == newItem
    }

}