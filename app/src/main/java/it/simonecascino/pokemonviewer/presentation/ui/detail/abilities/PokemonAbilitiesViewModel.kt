package it.simonecascino.pokemonviewer.presentation.ui.detail.abilities

import com.ww.roxie.BaseAction
import com.ww.roxie.BaseState
import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.models.Ability
import it.simonecascino.pokemonviewer.domain.usecases.GetPokemonAbilitiesUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class PokemonAbilitiesViewModel(private val id: Long): BaseViewModel<Action, AbilitiesState>(), KoinComponent {

    override val initialState = AbilitiesState()

    private val getPokemonAbilitiesUseCase: GetPokemonAbilitiesUseCase by inject{ parametersOf(id) }

    private val reducer: Reducer<AbilitiesState, Change> = { state, change ->
        when (change) {
            is Change.Loading -> state.copy(
                isIdle = false,
                isLoading = true
            )
            is Change.Abilities -> state.copy(
                isLoading = false,
                abilities = change.abilities
            )
            is Change.Error -> state.copy(
                isLoading = false
            )
        }
    }

    private fun bindActions(){

        val abilityChange = actions.ofType<Action.GetAbilities>()
            .switchMap {
                getPokemonAbilitiesUseCase.abilities
                    .subscribeOn(Schedulers.io())
                    .map<Change> {
                        Timber.d("abilities are $it")
                        Change.Abilities(it) }
                    .defaultIfEmpty(Change.Abilities(emptyList()))
                    .startWith(Change.Loading)
            }

        disposables += abilityChange
            .scan(initialState, reducer)
            .filter{ !it.isIdle }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

    }

    init {
        bindActions()
        dispatch(Action.GetAbilities)
    }

}

sealed class Action: BaseAction {
    object GetAbilities: Action()
}

sealed class Change {
    object Loading: Change()
    data class Abilities(val abilities: List<Ability>): Change()
    data class Error(val throwable: Throwable?) : Change()
}

data class AbilitiesState(
    val abilities: List<Ability> = listOf(),
    val isIdle: Boolean = true,
    val isLoading: Boolean = false
) : BaseState