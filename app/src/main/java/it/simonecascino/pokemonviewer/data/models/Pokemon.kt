package it.simonecascino.pokemonviewer.data.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import it.simonecascino.pokemonviewer.data.database.entities.PokemonDetailsDb
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

@Parcelize
data class Pokemon(
    val id: Long,
    val name: String
): Parcelable{

    @IgnoredOnParcel
    val displayName = name.capitalize(Locale.getDefault())

    @IgnoredOnParcel
    val transitionName = "image_$id"

    @IgnoredOnParcel
    val artworkUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/$id.png"

}

data class PokemonDetails(
    val pokemon: Pokemon,
    val details: Details?
){

    data class Details(
        val baseExperience: Int,
        val height: Int,
        val isDefault: Boolean,
        val locationAreaEncounters: String,
        val order: Int,
        val weight: Int,
        val images: Images
    ){

        @Parcelize
        data class Images(
            val backDefault: String?,
            val backFemale: String?,
            val backShiny: String?,
            val backShinyFemale: String?,
            val frontDefault: String?,
            val frontFemale: String?,
            val frontShiny: String?,
            val frontShinyFemale: String?,
            val officialArtwork: String?
        ): Parcelable{

            fun toList(): List<String>{

                val images = ArrayList<String>(9)

                addIfNotNull(officialArtwork, images)
                addIfNotNull(backDefault, images)
                addIfNotNull(backFemale, images)
                addIfNotNull(backShiny, images)
                addIfNotNull(backShinyFemale, images)
                addIfNotNull(frontDefault, images)
                addIfNotNull(frontFemale, images)
                addIfNotNull(frontShiny, images)
                addIfNotNull(frontShinyFemale, images)

                return images

            }

            private fun addIfNotNull(url: String?, images: ArrayList<String>){

                if(url != null)
                    images.add(url)

            }

        }
    }

}