package it.simonecascino.pokemonviewer.data.repositories

import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.daos.AbilityDao
import it.simonecascino.pokemonviewer.data.models.Ability
import org.koin.core.KoinComponent
import org.koin.core.inject

interface PokemonAbilitiesRepository{
    val abilities: Observable<List<Ability>>
}

class PokemonAbilitiesRepositoryImpl(id: Long): PokemonAbilitiesRepository, KoinComponent {

    private val abilityDao: AbilityDao by inject()

    override val abilities = abilityDao.getAbilitiesByPokemonId(id).map {
        it.map { it.toBaseModel() }
    }

}