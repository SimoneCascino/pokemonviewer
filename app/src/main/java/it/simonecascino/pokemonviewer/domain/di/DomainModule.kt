package it.simonecascino.pokemonviewer.domain.di

import it.simonecascino.pokemonviewer.data.repositories.*
import org.koin.dsl.module

val domainModule = module {

    factory <PokemonRepository> { PokemonRepositoryImpl(get()) }

    factory <PokemonDetailRepository> { (id: Long) ->  PokemonDetailRepositoryImpl(id)}

    factory <PokemonStatsRepository> { (id: Long) ->  PokemonStatsRepositoryImpl(id)}

    factory <PokemonAbilitiesRepository> { (id: Long) ->  PokemonAbilitiesRepositoryImpl(id)}

    factory <PokemonMovesRepository> { (id: Long) ->  PokemonMovesRepositoryImpl(id)}

}