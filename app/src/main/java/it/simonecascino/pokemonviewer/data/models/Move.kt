package it.simonecascino.pokemonviewer.data.models

import java.util.*

data class Move(
    val id: Long,
    val name: String
){
    val displayName = name.capitalize(Locale.getDefault())
}