package it.simonecascino.pokemonviewer.presentation.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentGalleryBinding
import it.simonecascino.pokemonviewer.presentation.ui.gallery.image.ImageFragment


class GalleryFragment : Fragment() {

    private val args: GalleryFragmentArgs by navArgs()

    private lateinit var binding: FragmentGalleryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        binding.toolbar.title = args.title

        val pagerAdapter = GalleryPagerAdapter(this, args.images.toList().map { url ->
            ImageFragment.createInstance(url)
        })

        binding.viewPager.adapter = pagerAdapter

    }
}