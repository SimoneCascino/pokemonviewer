package it.simonecascino.pokemonviewer.presentation.utils

import android.animation.Animator
import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewAnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import java.util.Collections.max
import kotlin.math.max

fun Context.dpToPx(dp: Int): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics)
        .toInt()
}

fun Fragment.dpToPx(dp: Int): Int {
    return requireActivity().dpToPx(dp)
}

inline fun View.snackbar(message: String, action: String, crossinline actionCallback: () -> Unit): Snackbar {

    return Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
        .setAction(action) {
            actionCallback()
        }.apply {
            animationMode = Snackbar.ANIMATION_MODE_SLIDE
            show()
        }

}

inline fun <T> LifecycleOwner.observe(liveData: LiveData<T>, crossinline observer: (T) -> Unit){

    val observerOwner = if(this is Fragment)
        viewLifecycleOwner else this

    liveData.observe(observerOwner, {
        observer(it)
    })
}

fun View.setGone(){
    visibility = View.GONE
}

fun View.setVisible(){
    visibility = View.VISIBLE
}

fun View.setInvisible(){
    visibility = View.INVISIBLE
}

fun View.isVisible(): Boolean = visibility == View.VISIBLE

fun View.enterCircularReveal(){

    if(!isAttachedToWindow)
        return

    val centerX = width/2
    val centerY = height/2

    setVisible()

    ViewAnimationUtils.createCircularReveal(this, centerX, centerY, 0F,
        max(centerX, centerY) * 1.5F).start()
}

fun View.exitCircularReveal(){

    if(!isAttachedToWindow)
        return

    val centerX = width/2
    val centerY = height/2

    ViewAnimationUtils.createCircularReveal(this, centerX, centerY, max(centerX, centerY) *1.5F,
        0F).apply {

        addListener(object: Animator.AnimatorListener{

            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                setInvisible()
                removeAllListeners()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}

        })

    }.start()

}