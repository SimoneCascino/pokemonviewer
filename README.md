L'app è stata sviluppata rispettando i requisiti richiesti, in particolare:

* koin per la di;
* rxjava / rxkotlin;
* roxie per MVI.

Le api usate offrono molteplici informazioni, per la creazione del dettaglio ne ho usate solo alcune, ho pensato che ogni altra aggiunta
sarebbe stata superflua ai fini del test, trattandosi di cose che avrebbero prodotto componenti strutturalmente simili.