package it.simonecascino.pokemonviewer.data.network.pojos

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.base.Predicates.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class PokemonApiResponseTest{

    private val pokemonApiResponse = PokemonApiResponse(
        listOf(
            PokemonResponse("pokemon1", "www.google.it/1"),
            PokemonResponse("pokemon2", "www.google.it/2")
        )
    )

    @Test
    fun `check conversion to db model`(){

        val converted = pokemonApiResponse.toDbModel()

        converted.forEach {
            assertThat(
                it.id,
                `is`(not(equalTo(-1)))
            )
        }



    }

}