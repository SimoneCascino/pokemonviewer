package it.simonecascino.pokemonviewer.presentation.ui.detail.stats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentPokemonStatsBinding
import it.simonecascino.pokemonviewer.presentation.custom.SpaceItemDecoration
import it.simonecascino.pokemonviewer.presentation.utils.dpToPx
import it.simonecascino.pokemonviewer.presentation.utils.observe
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

private const val KEY_ID = "id"

class PokemonStatsFragment : Fragment() {

    private lateinit var binding: FragmentPokemonStatsBinding

    private val pokemonStatsViewModel: PokemonStatsViewModel by viewModel{ parametersOf(arguments?.getLong(KEY_ID) ?: -1L)  }

    companion object{

        @JvmStatic
        fun createInstance(id: Long) = PokemonStatsFragment().apply {
            arguments = Bundle(1).apply {
                putLong(KEY_ID, id)
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_stats, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PokemonStatsAdapter()
        binding.statsList.addItemDecoration(SpaceItemDecoration(dpToPx(16)))
        binding.statsList.adapter = adapter

        observe(pokemonStatsViewModel.observableState){
            adapter.submitList(it.stats)
        }

    }

}