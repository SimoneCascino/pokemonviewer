package it.simonecascino.pokemonviewer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import it.simonecascino.pokemonviewer.data.models.Move

@Entity(tableName = "move")
data class MoveDb (
    @PrimaryKey @ColumnInfo(name = "move_id") val id: Long,
    val name: String
){
    fun toBaseModel() = Move(id, name)

    fun toPokemonMoveModel(pokemonId: Long) = PokemonMoveDb(id, pokemonId)
}

@Entity(tableName = "pokemon_move",
    primaryKeys = ["move_ref_id", "pokemon_ref_id"]
)
data class PokemonMoveDb(
    @ColumnInfo(name = "move_ref_id") val moveId: Long,
    @ColumnInfo(name = "pokemon_ref_id") val pokemonId: Long
)