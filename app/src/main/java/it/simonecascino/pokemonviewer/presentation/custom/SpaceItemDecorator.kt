package it.simonecascino.pokemonviewer.presentation.custom

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SpaceItemDecoration(private val dp: Int): RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val layoutManager = parent.layoutManager

        val size = layoutManager?.itemCount ?: 0

        val position = layoutManager?.getPosition(view) ?: 0

        when (layoutManager) {

            is GridLayoutManager -> addOffset(outRect, size, layoutManager.spanCount, position)
            is LinearLayoutManager -> addOffset(outRect, size, 1, position)
            else -> throw UnsupportedOperationException("Unsupported layout manager")
        }

    }

    private fun addOffset(outRect: Rect, size: Int, spanCount: Int, position: Int){

        val rest = position%spanCount

        var rows: Int = size/spanCount

        val rowRest = size%spanCount

        if(rowRest > 0)
            rows ++

        var currentRow: Int = (position + 1)/spanCount

        val currentRowRest = (position + 1)%spanCount

        if(currentRowRest > 0)
           currentRow ++

        val leftOffset = if(rest == 0)
            dp
        else dp/2

        val topOffset = if (position < spanCount)
            dp
        else dp/2

        val rightOffset = if(rest == spanCount - 1)
            dp
        else dp/2

        val bottomOffset = if(currentRow!=rows)
            dp/2
        else dp

        outRect.set(leftOffset, topOffset, rightOffset, bottomOffset)

    }


}