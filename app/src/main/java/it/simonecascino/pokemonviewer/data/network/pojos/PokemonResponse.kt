package it.simonecascino.pokemonviewer.data.network.pojos

import androidx.core.net.toUri
import it.simonecascino.pokemonviewer.data.database.entities.SimplePokemonDb
import it.simonecascino.pokemonviewer.data.network.utils.extractId

data class PokemonResponse (
    val name: String,
    val url: String
){

    val id = url.toUri().extractId()

    fun toDbModel() = SimplePokemonDb(id, name)
}

data class PokemonApiResponse(
    val results: List<PokemonResponse>
){
    fun toDbModel() = results.map {
        it.toDbModel()
    }
}