package it.simonecascino.pokemonviewer.presentation.ui.detail.moves

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentPokemonMovesBinding
import it.simonecascino.pokemonviewer.presentation.custom.SpaceItemDecoration
import it.simonecascino.pokemonviewer.presentation.utils.observe
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

private const val KEY_ID = "id"

class PokemonMovesFragment : Fragment() {

    private lateinit var binding: FragmentPokemonMovesBinding

    private val pokemonMovesViewModel: PokemonMovesViewModel by viewModel{ parametersOf(arguments?.getLong(KEY_ID) ?: -1) }

    companion object{

        @JvmStatic
        fun createInstance(id: Long) = PokemonMovesFragment().apply {
            arguments = Bundle(1).apply {
                putLong(KEY_ID, id)
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_moves, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.movesList.addItemDecoration(SpaceItemDecoration(16))

        val adapter = PokemonMovesListAdapter()

        binding.movesList.adapter = adapter

        observe(pokemonMovesViewModel.observableState){
            adapter.submitList(it.moves)
        }

    }
}