package it.simonecascino.pokemonviewer.data.repositories

import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.daos.MoveDao
import it.simonecascino.pokemonviewer.data.models.Move
import org.koin.core.KoinComponent
import org.koin.core.inject

interface PokemonMovesRepository {
    val moves: Observable<List<Move>>
}

class PokemonMovesRepositoryImpl(id: Long): PokemonMovesRepository, KoinComponent{

    private val moveDao: MoveDao by inject()

    override val moves = moveDao.getMovesByPokemonId(id).map {
        it.map { it.toBaseModel() }
    }

}

