package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import it.simonecascino.pokemonviewer.data.database.entities.PokemonMoveDb

@Dao
interface PokemonMoveDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPokemonMove(pokemonMoves: List<PokemonMoveDb>)

}