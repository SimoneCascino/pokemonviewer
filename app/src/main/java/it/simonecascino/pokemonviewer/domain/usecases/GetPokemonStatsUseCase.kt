package it.simonecascino.pokemonviewer.domain.usecases

import it.simonecascino.pokemonviewer.data.repositories.PokemonStatsRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class GetPokemonStatsUseCase (private val id: Long): KoinComponent{

    private val repository: PokemonStatsRepository by inject { parametersOf(id) }

    val stats = repository.stats

}