package it.simonecascino.pokemonviewer.presentation.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.data.models.Pokemon
import it.simonecascino.pokemonviewer.databinding.ItemPokemonBinding

class PokemonListAdapter(private val listener: ItemClickListener): PagingDataAdapter<Pokemon, PokemonListAdapter.ViewHolder>(PokemonDiff()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    class ViewHolder private constructor(private val binding: ItemPokemonBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_pokemon, parent, false))
        }

        fun bind(pokemon: Pokemon?, listener: ItemClickListener){

            pokemon?.let {
                binding.pokemon = it
                binding.listener = listener
                binding.executePendingBindings()
            }

        }

    }

    private class PokemonDiff: DiffUtil.ItemCallback<Pokemon>(){
        override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon) = oldItem == newItem
    }

    class ItemClickListener(private val callback: (pokemon: Pokemon, view: View) -> Unit){

        fun onClick(pokemon: Pokemon, view: View){
            callback(pokemon, view)
        }

    }

}