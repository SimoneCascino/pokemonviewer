package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.entities.MoveDb

@Dao
interface MoveDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMove(moves: List<MoveDb>)

    @Query("select * from move inner join pokemon_move on move.move_id = pokemon_move.move_ref_id where pokemon_move.pokemon_ref_id = :id")
    fun getMovesByPokemonId(id: Long): Observable<List<MoveDb>>

}