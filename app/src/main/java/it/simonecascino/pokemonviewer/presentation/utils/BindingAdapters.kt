package it.simonecascino.pokemonviewer.presentation.utils

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlin.math.max


@BindingAdapter("loadImage")
fun ImageView.loadImage(url: String?){

    if(isVisible())
        return

    url?.let {

        val startTime = System.currentTimeMillis()
        val expectedTime = 500

        Glide.with(this).load(url).addListener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {

                val time = System.currentTimeMillis() - startTime

                postDelayed({

                    enterCircularReveal()

                }, max(0, expectedTime - time))

                return false
            }

        }) .into(this)
    }
}

@BindingAdapter("simpleLoad")
fun ImageView.simpleLoad(url: String?){
    url?.let {
        Glide.with(this).load(url).into(this)
    }
}

@BindingAdapter("fullText", "value", requireAll = true)
fun TextView.setBoldHeadText(fullText: String?, value: String?){

    if(value != null && fullText != null){

        val spannableText = SpannableString(fullText)

        val boldSpan = StyleSpan(Typeface.BOLD)

        spannableText.setSpan(boldSpan, 0, fullText.length - value.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        text = spannableText

    }

}