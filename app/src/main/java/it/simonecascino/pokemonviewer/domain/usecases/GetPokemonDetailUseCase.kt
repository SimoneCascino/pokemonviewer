package it.simonecascino.pokemonviewer.domain.usecases

import it.simonecascino.pokemonviewer.data.repositories.PokemonDetailRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class GetPokemonDetailUseCase(private val id: Long): KoinComponent {

    private val repository: PokemonDetailRepository by inject { parametersOf(id) }

    val pokemonDetail = repository.pokemon

    val pokemonApi = repository.pokemonApi

}