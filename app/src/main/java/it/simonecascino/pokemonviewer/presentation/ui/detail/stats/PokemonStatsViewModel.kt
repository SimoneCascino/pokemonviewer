package it.simonecascino.pokemonviewer.presentation.ui.detail.stats

import com.ww.roxie.BaseAction
import com.ww.roxie.BaseState
import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.models.Stat
import it.simonecascino.pokemonviewer.domain.usecases.GetPokemonStatsUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class PokemonStatsViewModel(
    private val id: Long
): BaseViewModel<Action, StatsState>(), KoinComponent {

    override val initialState = StatsState()

    private val getPokemonStatsUseCase: GetPokemonStatsUseCase by inject{ parametersOf(id) }

    private val reducer: Reducer<StatsState, Change> = { state, change ->
        when (change) {
            is Change.Loading -> state.copy(
                isIdle = false,
                isLoading = true
            )
            is Change.Stats -> state.copy(
                isLoading = false,
                stats = change.stats
            )
            is Change.Error -> state.copy(
                isLoading = false
            )
        }
    }

    private fun bindActions(){

        val statsChange = actions.ofType<Action.GetStats>()
            .switchMap {
                getPokemonStatsUseCase.stats
                    .subscribeOn(Schedulers.io())
                    .map<Change> { Change.Stats(it) }
                    .defaultIfEmpty(Change.Stats(emptyList()))
                    .startWith(Change.Loading)
            }

        disposables += statsChange
            .scan(initialState, reducer)
            .filter{ !it.isIdle }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

    }

    init {
        bindActions()
        dispatch(Action.GetStats)
    }

}

sealed class Action: BaseAction {
    object GetStats: Action()
}

sealed class Change {
    object Loading: Change()
    data class Stats(val stats: List<Stat>): Change()
    data class Error(val throwable: Throwable?) : Change()
}

data class StatsState(
    val stats: List<Stat> = listOf(),
    val isIdle: Boolean = true,
    val isLoading: Boolean = false
) : BaseState