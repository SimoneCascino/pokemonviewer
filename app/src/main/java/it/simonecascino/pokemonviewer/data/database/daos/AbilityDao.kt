package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.entities.AbilityDb

@Dao
interface AbilityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAbilities(abilities: List<AbilityDb>)

    @Query("select * from ability inner join pokemon_ability on ability.ability_id = pokemon_ability.ability_ref_id where pokemon_ability.pokemon_ref_id = :id")
    fun getAbilitiesByPokemonId(id: Long): Observable<List<AbilityDb>>

}