package it.simonecascino.pokemonviewer.data.network.pojos

import androidx.core.net.toUri
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import it.simonecascino.pokemonviewer.data.database.entities.*
import it.simonecascino.pokemonviewer.data.network.utils.extractId

@JsonClass(generateAdapter = true)
data class PokemonDetailResponse (
    val id: Long,
    val name: String,
    @Json(name = "base_experience") val baseExperience: Int,
    val height: Int,
    @Json(name = "is_default") val isDefault: Boolean,
    @Json(name = "location_area_encounters") val locationAreaEncounters: String,
    val order: Int,
    val weight: Int,
    val abilities: List<AbilityObj>,
    @Json(name = "game_indices") val gameIndices: List<GameIndex>,
    @Json(name = "held_items") val heldItems: List<HeldItem>,
    val moves: List<MoveObj>,
    val sprites: Sprites,
    val stats: List<StatObj>
) {

    data class AbilityObj(
        @Json(name = "is_hidden") val isHidden: Boolean,
        val slot: Int,
        val ability: Ability
    ){

        data class Ability(
            val name: String,
            val url: String
        )

        fun toDbModel(): AbilityDb {

            return AbilityDb(
                ability.url.toUri().extractId(),
                ability.name,
                slot,
                isHidden
            )
        }

    }

    data class GameIndex(
        @Json(name = "game_index") val gameIndex: Int,
        val version: Version
    )

    data class HeldItem(
        val item: Item,
        @Json(name = "version_details") val versionDetails: List<VersionDetail>
    ){

        data class VersionDetail(
            val version: Version,
            val rarity: Int
        )

        data class Item(
            val name: String,
            val url: String
        )

    }

    data class Version(
        val name: String,
        val url: String
    )

    data class MoveObj(
        val move: Move
    ){

        data class Move(
            val name: String,
            val url: String
        )

        fun toDbModel() = MoveDb(
            move.url.toUri().extractId(),
            move.name
        )

    }

    data class Sprites(
        @Json(name = "back_default") val backDefault: String?,
        @Json(name = "back_female") val backFemale: String?,
        @Json(name = "back_shiny") val backShiny: String?,
        @Json(name = "back_shiny_female") val backShinyFemale: String?,
        @Json(name = "front_default") val frontDefault: String?,
        @Json(name = "front_female") val frontFemale: String?,
        @Json(name = "front_shiny") val frontShiny: String?,
        @Json(name = "front_shiny_female") val frontShinyFemale: String?,
        val other: Other
    ){
        data class Other(
            @Json(name = "official-artwork") val officialArtwork: OfficialArtwork
        ){

            data class OfficialArtwork(
                @Json(name = "front_default") val frontDefault: String
            )

        }
    }

    data class StatObj(
        val stat: Stat,
        @Json(name = "base_stat") val baseStat: Int,
        val effort: Int
    ){

        data class Stat(
            val name: String,
            val url: String
        )

        fun toDbModel(pokemonId: Long) = StatDb(
            pokemonId,
            stat.name,
            baseStat,
            effort
        )

    }

    fun toDbModel(): PokemonDb {

        return PokemonDb(
            id,
            name,
            PokemonDetailsDb(
                baseExperience,
                height,
                isDefault,
                locationAreaEncounters,
                order,
                weight,
                PokemonDetailsDb.ImagesDb(
                    sprites.backDefault,
                    sprites.backFemale,
                    sprites.backShiny,
                    sprites.backShinyFemale,
                    sprites.frontDefault,
                    sprites.frontFemale,
                    sprites.frontShiny,
                    sprites.frontShinyFemale,
                    sprites.other.officialArtwork.frontDefault
                )
            )
        )
    }

}


