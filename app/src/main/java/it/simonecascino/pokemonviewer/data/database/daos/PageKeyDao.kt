package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.*
import it.simonecascino.pokemonviewer.data.database.entities.PageKeyDb

@Dao
interface PageKeyDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPageKey(pageKeys: List<PageKeyDb>)

    @Query("select * from page_key where pokemon_page_id = :pokemonId")
    fun getPageKeyById(pokemonId: Long): PageKeyDb?

    @Query("delete from page_key")
    fun deleteKeys()

}