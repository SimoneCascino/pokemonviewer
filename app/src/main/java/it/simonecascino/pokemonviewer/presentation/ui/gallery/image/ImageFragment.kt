package it.simonecascino.pokemonviewer.presentation.ui.gallery.image

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.databinding.FragmentImageBinding

private const val KEY_URL = "url"

class ImageFragment : Fragment() {

    private lateinit var binding: FragmentImageBinding

    companion object {

        @JvmStatic
        fun createInstance(url: String) =
            ImageFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_URL, url)
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_image, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(KEY_URL)?.let { url ->
            binding.url = url
        }

    }
}