package it.simonecascino.pokemonviewer.data.network.pojos

import android.os.Build
import androidx.core.net.toUri
import androidx.test.ext.junit.runners.AndroidJUnit4
import it.simonecascino.pokemonviewer.data.network.utils.extractId
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class PokemonResponseTest{

    private val pokemons = listOf(
        PokemonResponse("pk1", "www.test.com/1"),
        PokemonResponse("pk2", "www.test.com/2"),
        PokemonResponse("pk3", "www.test.com/3"),
    )

    @Test
    fun `to db model function produce a valid PokemonDb`(){
        val pokemonResponse = pokemons[0]
        assertThat(pokemonResponse.toDbModel().id, `is`(not(equalTo(-1))))
    }

    @Test
    fun `pokemon must have valid id`(){
        pokemons.forEach {
            assertThat(it.url.toUri().extractId(), `is`(not(equalTo(-1))))
        }
    }

}