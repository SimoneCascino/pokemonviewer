package it.simonecascino.pokemonviewer.domain.usecases

import it.simonecascino.pokemonviewer.data.repositories.PokemonAbilitiesRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class GetPokemonAbilitiesUseCase(id: Long): KoinComponent {

    private val repository: PokemonAbilitiesRepository by inject { parametersOf(id) }

    val abilities = repository.abilities

}