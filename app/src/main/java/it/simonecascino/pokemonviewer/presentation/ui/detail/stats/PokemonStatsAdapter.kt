package it.simonecascino.pokemonviewer.presentation.ui.detail.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.pokemonviewer.R
import it.simonecascino.pokemonviewer.data.models.Stat
import it.simonecascino.pokemonviewer.databinding.ItemStatBinding

class PokemonStatsAdapter: ListAdapter<Stat, PokemonStatsAdapter.ViewHolder>(StatDiff()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(private val binding: ItemStatBinding):
        RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_stat, parent, false))
        }

        fun bind(stat: Stat){

            binding.stat = stat
            binding.executePendingBindings()

        }

    }

    private class StatDiff: DiffUtil.ItemCallback<Stat>(){
        override fun areItemsTheSame(oldItem: Stat, newItem: Stat) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Stat, newItem: Stat) = oldItem == newItem
    }

}