package it.simonecascino.pokemonviewer.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.database.AppDatabase
import it.simonecascino.pokemonviewer.data.database.entities.PageKeyDb
import it.simonecascino.pokemonviewer.data.database.entities.SimplePokemonDb
import it.simonecascino.pokemonviewer.data.database.entities.toPageKeys
import it.simonecascino.pokemonviewer.data.network.Api
import java.io.InvalidObjectException

private const val START_INDEX = 0
const val PAGE_SIZE = 60
private const val INVALID_PAGE = -1

@OptIn(ExperimentalPagingApi::class)
class PokemonListRemoteMediator(private val api: Api,
                                private val database: AppDatabase): RxRemoteMediator<Int, SimplePokemonDb>() {

    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<Int, SimplePokemonDb>
    ): Single<MediatorResult> {

        return Single.just(loadType).subscribeOn(Schedulers.io())

            .map {

                when (it) {
                    LoadType.REFRESH -> {
                        getPageKeyCloseToCurrentPosition(state)
                            ?.nextKey?.minus(PAGE_SIZE) ?: START_INDEX
                    }
                    LoadType.PREPEND -> {
                        (getPageKeyForFirstItem(state) ?: throw InvalidObjectException("Result is empty"))
                            .previousKey ?: INVALID_PAGE
                    }
                    LoadType.APPEND -> {
                        (getPageKeyForLastItem(state) ?: throw InvalidObjectException("Result is empty"))
                            .nextKey ?: INVALID_PAGE
                    }

                }

            }.flatMap { page ->

                if(page == INVALID_PAGE)
                    Single.just(MediatorResult.Success(endOfPaginationReached = true))

                else {

                    api.getPokemon(offset = page)
                        .map<MediatorResult> {

                            val pokemonsDb = it.toDbModel()
                            saveToDb(pokemonsDb, page, loadType)

                            MediatorResult.Success(endOfPaginationReached = it.results.isEmpty())

                        }.onErrorReturn {
                            MediatorResult.Error(it)
                        }

                }

            }.onErrorReturn {
                MediatorResult.Error(it)
            }

    }

    private fun saveToDb(pokemonsDb: List<SimplePokemonDb>, page: Int, loadType: LoadType){

        database.runInTransaction {

            if(loadType == LoadType.REFRESH){
                database.pokemonDao().deletePokemons()
            }

            database.pokemonDao().insertPokemons(pokemonsDb)

            val prevKey = if(page == 0) null else page - PAGE_SIZE
            val nextKey = if(pokemonsDb.isEmpty()) null else page + PAGE_SIZE

            database.pageKeyDao().insertPageKey(pokemonsDb.toPageKeys(prevKey, nextKey))
        }

    }

    private fun getPageKeyForLastItem(state: PagingState<Int, SimplePokemonDb>): PageKeyDb?{

        return state.pages.lastOrNull {
            it.data.isNotEmpty()
        }?.data?.lastOrNull()?.let {
            database.pageKeyDao().getPageKeyById(it.id)
        }

    }

    private fun getPageKeyForFirstItem(state: PagingState<Int, SimplePokemonDb>): PageKeyDb?{

        return state.pages.firstOrNull {
            it.data.isNotEmpty()
        }?.data?.firstOrNull()?.let {
            database.pageKeyDao().getPageKeyById(it.id)
        }

    }

    private fun getPageKeyCloseToCurrentPosition(state: PagingState<Int, SimplePokemonDb>): PageKeyDb?{

        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                database.pageKeyDao().getPageKeyById(id)
            }
        }

    }

    fun getPagingSourceFactory() = database.pokemonDao().getPokemons()

}