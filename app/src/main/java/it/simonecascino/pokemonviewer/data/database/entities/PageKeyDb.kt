package it.simonecascino.pokemonviewer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "page_key",
    foreignKeys = [
        ForeignKey(
            entity = PokemonDb::class,
            parentColumns = arrayOf("pokemon_id"),
            childColumns = arrayOf("pokemon_page_id"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class PageKeyDb(
    @PrimaryKey @ColumnInfo(name = "pokemon_page_id") val pokemonId: Long,
    @ColumnInfo(name = "next_key") val nextKey: Int?,
    @ColumnInfo(name = "previous_key") val previousKey: Int?
)