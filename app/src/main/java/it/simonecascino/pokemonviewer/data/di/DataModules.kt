package it.simonecascino.pokemonviewer.data.di

import androidx.room.Room
import it.simonecascino.pokemonviewer.data.PokemonListRemoteMediator
import it.simonecascino.pokemonviewer.data.database.AppDatabase
import it.simonecascino.pokemonviewer.data.database.daos.StatDao
import it.simonecascino.pokemonviewer.data.network.*
import org.koin.dsl.module

val networkModule = module {

    factory { provideLoggingInterceptor() }

    factory { provideConverterFactory() }

    factory { provideHttpClient(get()) }

    factory { provideRetrofit(get(), get())}

    single { provideApi(get()) }

}

val databaseModule = module {

    single { Room.databaseBuilder(get(), AppDatabase::class.java, "pokemon_db").build() }

    single { get<AppDatabase>().pokemonDao() }

    single { get<AppDatabase>().pageKeyDao() }

    single { get<AppDatabase>().statDao() }

    single { get<AppDatabase>().abilityDao() }

    single { get<AppDatabase>().moveDao() }

    /*

    //for test
    single (named("fake")) { Room.inMemoryDatabaseBuilder(get(), AppDatabase::class.java).build() }

    single (named("fake_dao")) { get<AppDatabase>(named("fake")).pokemonDao() }

     */
}

val remoteMediatorModule = module {

    factory { PokemonListRemoteMediator(get(), get()) }

}