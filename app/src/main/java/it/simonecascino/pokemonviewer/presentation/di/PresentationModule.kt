package it.simonecascino.pokemonviewer.presentation.di

import it.simonecascino.pokemonviewer.domain.usecases.*
import it.simonecascino.pokemonviewer.presentation.ui.detail.PokemonDetailViewModel
import it.simonecascino.pokemonviewer.presentation.ui.detail.abilities.PokemonAbilitiesViewModel
import it.simonecascino.pokemonviewer.presentation.ui.detail.moves.PokemonMovesViewModel
import it.simonecascino.pokemonviewer.presentation.ui.detail.stats.PokemonStatsViewModel
import it.simonecascino.pokemonviewer.presentation.ui.list.ListViewState
import it.simonecascino.pokemonviewer.presentation.ui.list.PokemonListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    factory { GetPokemonListUseCase(get()) }

    factory { (id: Long) -> GetPokemonDetailUseCase(id) }

    factory { (id: Long) -> GetPokemonStatsUseCase(id) }

    factory { (id: Long) -> GetPokemonAbilitiesUseCase(id) }

    factory { (id: Long) -> GetPokemonMovesUseCase(id) }

    factory { ListViewState(isIdle = true) }

    viewModel { PokemonListViewModel(get(), get()) }

    viewModel { (id: Long) -> PokemonDetailViewModel(id) }

    viewModel { (id: Long) -> PokemonStatsViewModel(id) }

    viewModel { (id: Long) -> PokemonAbilitiesViewModel(id) }

    viewModel { (id: Long) -> PokemonMovesViewModel(id) }

}