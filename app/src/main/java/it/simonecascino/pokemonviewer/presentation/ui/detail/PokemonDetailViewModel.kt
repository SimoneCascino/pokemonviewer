package it.simonecascino.pokemonviewer.presentation.ui.detail

import com.ww.roxie.BaseAction
import com.ww.roxie.BaseState
import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import it.simonecascino.pokemonviewer.data.models.PokemonDetails
import it.simonecascino.pokemonviewer.domain.usecases.GetPokemonDetailUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class PokemonDetailViewModel(
    private val id: Long,
): BaseViewModel<Action, DetailState>(), KoinComponent{

    private val getPokemonDetailUseCase: GetPokemonDetailUseCase by inject{ parametersOf(id) }

    override val initialState = DetailState()

    private val reducer: Reducer<DetailState, Change> = { state, change ->
        when (change) {
            is Change.Loading -> state.copy(
                isIdle = false,
                isLoading = true,
                value = null,
                isError = false
            )
            is Change.Details -> state.copy(
                isLoading = false,
                value = change.value
            )
            is Change.Error -> state.copy(
                isLoading = false,
                isError = true
            )
            Change.ApiComplete -> state.copy(
                isLoading = false
            )
            Change.Gallery -> state.copy(
                showGallery = true
            )
            Change.HandleGallery -> state.copy(
                showGallery = false
            )
        }
    }

    private fun bindActions(){

        val dbChange = actions.ofType<Action.GetDetail>()
            .switchMap {
                getPokemonDetailUseCase.pokemonDetail
                    .subscribeOn(Schedulers.io())
                    .map {

                        if(it.details == null){
                            Change.Loading
                        }

                        else Change.Details(it)
                    }
                    .defaultIfEmpty(Change.Details(null))
                    .startWith(Change.Loading)
            }


        val apiChange = actions.ofType<Action.GetDetail>()
            .switchMap {
                getPokemonDetailUseCase.pokemonApi
                    .toObservable()
                    .map<Change> {
                        Change.ApiComplete
                    }
                    .onErrorReturn {
                        Timber.d(it)
                        Change.Error(it) }
                    .startWith(Change.Loading)
            }

        val galleryChange = actions.ofType<Action.ShowGallery>()
            .map {
                Change.Gallery
            }

        val handleShowGalleryChange = actions.ofType<Action.HandleShowGallery>()
            .map {
                Change.HandleGallery
            }

        disposables += Observable.merge(dbChange, apiChange, galleryChange, handleShowGalleryChange)
            .scan(initialState, reducer)
            .filter{ !it.isIdle }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue, Timber::e)

    }

    init {
        bindActions()
        dispatch(Action.GetDetail)
    }

}

sealed class Action: BaseAction {
    object GetDetail: Action()
    object ShowGallery: Action()
    object HandleShowGallery: Action()
}

sealed class Change {
    object Loading: Change()
    object ApiComplete: Change()
    object Gallery: Change()
    object HandleGallery: Change()
    data class Details(val value: PokemonDetails?): Change()
    data class Error(val throwable: Throwable?) : Change()
}

data class DetailState(
    val value: PokemonDetails? = null,
    val isIdle: Boolean = true,
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val showGallery: Boolean = false
) : BaseState{

    fun imagesAvailable(): Boolean{

        val images = value?.details?.images

        if(images != null){

            if(images.backDefault != null ||
                    images.backFemale != null ||
                    images.backShiny != null ||
                    images.backShinyFemale != null ||
                    images.frontDefault != null ||
                    images.frontFemale != null ||
                    images.frontShiny != null ||
                    images.frontShinyFemale != null ||
                    images.officialArtwork != null)
                return true

        }

        return false

    }

}