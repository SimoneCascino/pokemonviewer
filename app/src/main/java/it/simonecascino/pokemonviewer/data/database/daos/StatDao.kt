package it.simonecascino.pokemonviewer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable
import it.simonecascino.pokemonviewer.data.database.entities.StatDb

@Dao
interface StatDao{

    @Query("select * from stat where pokemon_stat_id = :id")
    fun getStatsById(id: Long): Observable<List<StatDb>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertStats(stats: List<StatDb>)

}