package it.simonecascino.pokemonviewer.data.database.utils

import it.simonecascino.pokemonviewer.data.models.utils.BaseModel

interface DbModel <out T: BaseModel>{
    fun toBaseModel(): T
}