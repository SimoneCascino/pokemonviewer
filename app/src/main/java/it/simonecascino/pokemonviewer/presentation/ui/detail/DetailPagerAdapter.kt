package it.simonecascino.pokemonviewer.presentation.ui.detail

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class DetailPagerAdapter(parent: Fragment, private val fragments: List<Fragment>): FragmentStateAdapter(parent) {

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int) = fragments[position]
}