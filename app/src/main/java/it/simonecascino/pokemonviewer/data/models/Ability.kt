package it.simonecascino.pokemonviewer.data.models

import java.util.*

data class Ability(
    val id: Long,
    val name: String,
    val slot: Int,
    val isHidden: Boolean
){
    val displayName = name.capitalize(Locale.getDefault())
}